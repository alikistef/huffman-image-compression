function dict = getDict( node, vec, dict )
    if size(node.pixel_value, 2) == 1
        dict(end+1, :) = {node.pixel_value, vec};
    else
        vec(end+1) = 1;
        dict = getDict(node.left, vec, dict);
        vec(end) = 0;
        dict = getDict(node.right, vec, dict);
    end
end
